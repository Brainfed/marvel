<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 19/05/2016
 * Time: 1:01 PM
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportComicsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('import:comics')
            ->setDescription('Imports comics from the Marvel API and stores them locally')
            ->addOption(
                'commit',
                null,
                InputOption::VALUE_NONE,
                'If set then it runs for real and adds data to the database'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $output->writeln('Import comics from the Marvel API');

        if ($input->getOption('commit')) {
            // Just DO IT!
            $total = $this->getContainer()->get('comics')->populateLocalComicData(true);
            $this->getContainer()->get('doctrine')->getEntityManager()->flush();
        } else {
            // Just pretend to do it
            $total = $this->getContainer()->get('comics')->populateLocalComicData();
        }

        $output->writeln('Total comics: '.$total);

    }

}