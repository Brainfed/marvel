<?php
/**
 * Created by PhpStorm.
 * User: matthewhamm
 * Date: 30/04/2016
 * Time: 2:49 PM
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TrueFalseType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
                'true' => 'True',
                'false' => 'False'
            )
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}