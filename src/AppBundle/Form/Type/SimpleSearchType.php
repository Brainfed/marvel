<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 17/05/2016
 * Time: 12:58 PM
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SimpleSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search', TextType::class, array('required' => true))
            ->add('searchKind', SearchKindType::class, array(
                'label_attr' => array(
                    'class' => 'radio-inline'
                )
            ))
            ->add('submit', SubmitType::class);
    }
}