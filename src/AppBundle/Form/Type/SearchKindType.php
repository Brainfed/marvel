<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 17/05/2016
 * Time: 1:00 PM
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SearchKindType extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
                'Series' => 'series',
                'Event' => 'event'
            ),
            'expanded' => true,
            'multiple' => false,
            'choice_attr' => function($val, $key, $index) {
                return ['class' => 'radio-inline'];
            },
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

}