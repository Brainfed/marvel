<?php
/**
 * Created by PhpStorm.
 * User: matthewhamm
 * Date: 30/04/2016
 * Time: 2:41 PM
 */

namespace AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FormatTypeType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
                'comic' => 'Comic',
                'collection' => 'Collection'
            )
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}