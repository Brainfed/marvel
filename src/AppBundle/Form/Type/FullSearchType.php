<?php
/**
 * Created by PhpStorm.
 * User: matthewhamm
 * Date: 30/04/2016
 * Time: 2:52 PM
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class FullSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('required' => false))
            ->add('titleStartsWith', TextType::class, array('required' => false))
            ->add('issue_number', IntegerType::class, array('required' => false))
            ->add('format', FormatType::class, array('required' => false))
            ->add('formatType', FormatTypeType::class, array('required' => false))
            ->add('noVariants', TrueFalseType::class, array('required' => false))
            ->add('dateFrom', DateType::class, array('required' => false))
            ->add('dateTo', DateType::class, array('required' => false))
            ->add('startYear', IntegerType::class, array('required' => false))
            ->add('diamondCode', TextType::class, array('required' => false))
            ->add('digitalId', IntegerType::class, array('required' => false))
            ->add('upc', TextType::class, array('required' => false))
            ->add('isbn', TextType::class, array('required' => false))
            ->add('ean', TextType::class, array('required' => false))
            ->add('issn', TextType::class, array('required' => false))
            ->add('hasDigitalIssue', TrueFalseType::class, array('required' => false))
            ->add('modifiedSince', DateType::class, array('required' => false))
            ->add('send', SubmitType::class)
        ;
    }
}