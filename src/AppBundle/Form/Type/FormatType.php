<?php
/**
 * Created by PhpStorm.
 * User: matthewhamm
 * Date: 30/04/2016
 * Time: 2:36 PM
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FormatType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => array(
                'comic' => 'Comic',
                'magazine' => 'Magazine',
                'trade paperback' => 'Trade Paperback',
                'hardcover' => 'Hardcover',
                'digest' => 'Digest',
                'graphic novel' => 'Graphic Novel',
                'digital comic' => 'Digital Comic',
                'infinite comic' => 'Infinite Comic'
            )
        ));
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}