<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\SearchKind;
use AppBundle\Form\Type\SimpleSearchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\Type\SearchKindType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Homepage
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(SimpleSearchType::class, null, array(
            'action' => $this->generateUrl('search'),
            'method' => 'GET'
        ));
        $form->handleRequest($request);
        $pageData = array(
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'form' => $form->createView()
        );
        return $this->render('default/index.html.twig', $pageData);
    }

    /**
     * About page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function aboutAction()
    {
        return $this->render('default/about.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..')
        ]);
    }

}
