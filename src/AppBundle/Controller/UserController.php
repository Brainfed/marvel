<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 12/05/2016
 * Time: 10:18 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Comic;
use AppBundle\Entity\UserComic;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{

    public function myComicsAction() {
        $user = $this->getUser();
        /**
         * Get comics and put into sections
         */
        $userComics = $this->getDoctrine()->getRepository('AppBundle:UserComic')->findBy(array(
            'user' => $user
        ));
        $myComics = $this->get('my_comics')->sortMyComics($userComics);
        return $this->render('user/my-comics.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'comicsRead' => $myComics['comicsRead'],
            'comicsWatched' => $myComics['comicsWatched'],
            'comicsFavourited' => $myComics['comicsFavourited']
        ]);
    }

}