<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 12/05/2016
 * Time: 3:37 PM
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;

class AdminController extends Controller
{
    public function adminToolsAction() {
        $pageData = array(
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..')
        );
        return $this->render('admin/admin-tools.html.twig', $pageData);
    }

    public function findOrphanComicsAction() {
        $orphanComics = $this->getDoctrine()->getRepository('AppBundle:UserComic')->findOrphanComics();
        $pageData = array(
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'orphans' => $orphanComics
        );
        return $this->render('admin/orphan-comics.html.twig', $pageData);
    }

    public function deleteOrphanComicsAction() {
        $this->getDoctrine()->getRepository('AppBundle:UserComic')->deleteOrphanComics();
        return $this->redirectToRoute('orphans');
    }

    public function fetchOrphanComicsAction() {
        $orphanComics = $this->getDoctrine()->getRepository('AppBundle:UserComic')->findOrphanComics();
        foreach ($orphanComics as $orphanComic) {
            try {
                $comicDetails = $this->get('marvel_api_search')->comicSearch($orphanComic['comicId']);
                $this->getDoctrine()->getRepository('AppBundle:Comic')->addComic( $comicDetails);
                $this->getDoctrine()->getEntityManager()->flush();
            } catch(\Exception $e) {
                error_log($e->getMessage());
            }
        }
        return $this->redirectToRoute('orphans');
    }

    public function populateLocalComicsAction() {
        $total = $this->get('comics')->populateLocalComicData();

        $pageData = array(
            'total' => $total,
        );

        return $this->render('admin/import-comics.html.twig', $pageData);
    }

}