<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 12/05/2016
 * Time: 10:16 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Comic;
use AppBundle\Entity\UserComic;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ComicController extends Controller
{

    /**
     * View a specific comic
     * @param $comicId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function comicAction($comicId) {
        $comicDetails = $this->get('marvel_api_search')->comicSearch($comicId);

        /**
         * Determine if comic is My Comics
         */
        $user = $this->getUser();
        $read = false;
        $watched = false;
        $favourited = false;
        $rating = false;
        $userComic = $this->getDoctrine()->getRepository('AppBundle:UserComic')->findOneBy(array('comicId' => $comicId, 'user' => $user));
        if ($userComic) {
            $read = $userComic->getIsRead();
            $watched = $userComic->getWatch();
            $favourited = $userComic->getFavourite();
            $rating = $userComic->getRating();
        }
        return $this->render('comic/comic.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'comic' => $comicDetails->data->results,
            'read' => $read,
            'watched' => $watched,
            'favourited' => $favourited,
            'rating' => $rating,
            'attribution' => $comicDetails->attributionHTML
        ]);
    }

    /**
     * Add a comic's basic details to the database
     * @param integer $comicId
     * @param ObjectManager $em
     */
    private function addComicToDatabase($comicId, $em) {
        $comic = $this->getDoctrine()->getRepository('AppBundle:Comic')->findOneBy(
            array('marvelId' => $comicId)
        );
        if (!$comic) {
            // Grab it from the Marvel API
            $comicDetails = $this->get('marvel_api_search')->comicSearch($comicId);
            $this->getDoctrine()->getRepository('AppBundle:Comic')->addComic($comicDetails->data->results[0]);
            $this->getDoctrine()->getEntityManager()->flush();
        }
    }

    /**
     * Add a comic to user's My Comics
     * @param $comicId
     * @param $action
     * @param $rating
     * @param $return
     * @param $returnId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addComicAction($comicId, $action, $rating, $return, $returnId) {
        $user = $this->getUser();
        if (!$user) {
            $url = $this->generateUrl('fos_user_security_login');
            return RedirectResponse::create($url);
        }
        $em = $this->getDoctrine()->getManager();

        $userComic = $this->getDoctrine()->getRepository('AppBundle:UserComic')
            ->findOneBy(array('comicId' => $comicId, 'user' => $user));
        if (!$userComic) {
            $userComic = new UserComic();
            $userComic->setComicId($comicId);
            $userComic->setUser($user);
        }

        switch($action) {
            case 'read':
                $userComic->getIsRead() ? $userComic->setIsRead(false) : $userComic->setIsRead(true);
                break;
            case 'watch':
                $userComic->getWatch() ? $userComic->setWatch(false) : $userComic->setWatch(true);
                break;
            case 'favourite':
                $userComic->getFavourite() ? $userComic->setFavourite(false) : $userComic->setFavourite(true);
                break;
            case 'rating':
                $userComic->setRating($rating);
        }

        $em->persist($userComic);

        /**
         * If we don't have basic info for this issue in the database then add it
         */
        $this->addComicToDatabase($comicId, $em);

        $em->flush();

        $url = $this->generateUrl($return, array($return.'Id' => $returnId));
        return RedirectResponse::create($url);
    }

    /**
     * Remove a comic from user's My Comics
     * @param $comicId
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    public function removeComicAction($comicId) {
        $user = $this->getUser();
        $userComic = $this->getDoctrine()->getRepository('AppBundle:UserComic')
            ->findOneBy(array('comicId' => $comicId, 'user' => $user));
        if ($userComic) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($userComic);
            $em->flush();
        }
        $url = $this->generateUrl('comic', array('comicId' => $comicId));
        return RedirectResponse::create($url);
    }

}