<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 12/05/2016
 * Time: 10:14 AM
 */

namespace AppBundle\Controller;

use AppBundle\Form\Type\SimpleSearchType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\FullSearchType;

class SearchController extends Controller
{

    /**
     * Full comic search with all Marvel API parameters available
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fullSearchAction($page, Request $request) {
        $defaultData = array(
            'format' => 'comic'
        );
        $form = $this->createForm(FullSearchType::class, $defaultData, array(
            'method' => 'GET',
        ));
        $form->handleRequest($request);
        $pageData = array(
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'form' => $form->createView()
        );
        if ($form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            $searchResults = $this->get('marvel_api_search')->fullComicSearch($data);
            $pageData = $this->get('page_data')->parsePageData($data['search'], $searchResults, $page);
        }
        return $this->render('search/full-comic-search.html.twig', $pageData);
    }

    public function searchAction($page, Request $request) {
        $form = $this->createForm(SimpleSearchType::class, null, array(
            'method' => 'GET'
        ));
        $form->handleRequest($request);
        $pageData = array(
            'form' => $form->createView()
        );
        if ($form->isValid()) {
            $data = $form->getData();
            switch ($data['searchKind']) {
                case 'event':
                    $searchResults = $this->get('marvel_api_search')->eventSearch($data['search'], $page);

                    break;
                case 'series':
                default:
                    $searchResults = $this->get('marvel_api_search')->seriesSearch($data['search'], $page);
                    break;
            }
            $pageData = $this->get('page_data')->parsePageData($data['search'], $searchResults, $page, null, $data['searchKind']);
        }
        return $this->render('search/search-results.html.twig', $pageData);
    }

}