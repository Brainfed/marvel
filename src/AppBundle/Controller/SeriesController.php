<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 12/05/2016
 * Time: 10:59 AM
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SeriesController extends Controller
{

    /**
     * View a specific series
     * @param $seriesId
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seriesAction($seriesId, $page) {
        $seriesDetails = $this->get('marvel_api_search')->fetchSeries($seriesId);
        $comicDetails = $this->get('marvel_api_search')->comicSeriesSearch($seriesId, $page);
        /* Fetch comic details */
        $comics = $this->get('comics')->fetchComicDetails($comicDetails);
        $pageData = $this->get('page_data')->parsePageData($seriesDetails->data->results[0]->title, $comicDetails, $page, $comics, 'series');
        return $this->render('series/series.html.twig', $pageData);
    }

}