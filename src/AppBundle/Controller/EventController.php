<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 17/05/2016
 * Time: 1:24 PM
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EventController extends Controller
{

    /**
     * View a specific event
     * @param $marvelId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function eventAction($eventId, $page) {
        $eventDetails = $this->get('marvel_api_search')->fetchEvent($eventId);
        $comicDetails = $this->get('marvel_api_search')->comicEventSearch($eventId, $page);
        /* Fetch comic details */
        $comics = $this->get('comics')->fetchComicDetails($comicDetails);
        $pageData = $this->get('page_data')->parsePageData($eventDetails->data->results[0]->title, $eventDetails, $page, $comics, 'event');
        return $this->render('event/event.html.twig', $pageData);
    }

}