<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 5/05/2016
 * Time: 4:08 PM
 */

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

class User extends BaseUser
{
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}
