<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 12/05/2016
 * Time: 4:28 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ComicRepository extends EntityRepository
{

    public function addComic($comicDetails) {
        // And then add it!
        $comic = new Comic();
        $comic->setMarvelId($comicDetails->id);
        $comic->setTitle($comicDetails->title);
        $comic->setIssueNumber($comicDetails->issueNumber);
        $comic->setSeries($comicDetails->series->name);
        $this->getEntityManager()->persist($comic);
    }

}