<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 10/05/2016
 * Time: 5:25 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Comic
{

    private $id;

    private $marvelId;

    private $title;

    private $issueNumber;

    private $series;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marvelId
     *
     * @param integer $marvelId
     *
     * @return Comic
     */
    public function setMarvelId($marvelId)
    {
        $this->marvelId = $marvelId;

        return $this;
    }

    /**
     * Get marvelId
     *
     * @return integer
     */
    public function getMarvelId()
    {
        return $this->marvelId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Comic
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set issueNumber
     *
     * @param integer $issueNumber
     *
     * @return Comic
     */
    public function setIssueNumber($issueNumber)
    {
        $this->issueNumber = $issueNumber;

        return $this;
    }

    /**
     * Get issueNumber
     *
     * @return integer
     */
    public function getIssueNumber()
    {
        return $this->issueNumber;
    }

    /**
     * Set series
     *
     * @param string $series
     *
     * @return Comic
     */
    public function setSeries($series)
    {
        $this->series = $series;

        return $this;
    }

    /**
     * Get series
     *
     * @return string
     */
    public function getSeries()
    {
        return $this->series;
    }
}
