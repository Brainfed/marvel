<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 6/05/2016
 * Time: 10:45 AM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

class UserComic
{
    private $id;

    private $user;

    private $comicId;

    private $isRead;

    private $watch;

    private $favourite;

    private $rating;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comicId
     *
     * @param integer $comicId
     *
     * @return UserComic
     */
    public function setComicId($comicId)
    {
        $this->comicId = $comicId;

        return $this;
    }

    /**
     * Get comicId
     *
     * @return integer
     */
    public function getComicId()
    {
        return $this->comicId;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return UserComic
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set watch
     *
     * @param boolean $watch
     *
     * @return UserComic
     */
    public function setWatch($watch)
    {
        $this->watch = $watch;

        return $this;
    }

    /**
     * Get watch
     *
     * @return boolean
     */
    public function getWatch()
    {
        return $this->watch;
    }

    /**
     * Set favourite
     *
     * @param boolean $favourite
     *
     * @return UserComic
     */
    public function setFavourite($favourite)
    {
        $this->favourite = $favourite;

        return $this;
    }

    /**
     * Get favourite
     *
     * @return boolean
     */
    public function getFavourite()
    {
        return $this->favourite;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return UserComic
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     *
     * @return UserComic
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean
     */
    public function getIsRead()
    {
        return $this->isRead;
    }
}
