<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 11/05/2016
 * Time: 12:56 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UserComicRepository extends EntityRepository
{

    public function findOrphanComics() {
        $query = "
            SELECT uc.comicId
            FROM AppBundle:UserComic uc
            WHERE uc.comicId NOT
            IN (
                SELECT c.marvelId
                FROM AppBundle:Comic c
            )
        ";
        return $this->getEntityManager()
            ->createQuery($query)
            ->getResult();

    }

    public function deleteOrphanComics() {
        $query = "
            DELETE
            FROM AppBundle:UserComic uc
            WHERE uc.comicId NOT
            IN (
                SELECT c.marvelId
                FROM AppBundle:Comic c
            )
        ";
        return $this->getEntityManager()
            ->createQuery($query)
            ->getResult();

    }

}