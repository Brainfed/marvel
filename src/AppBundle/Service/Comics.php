<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 18/05/2016
 * Time: 4:33 PM
 */

namespace AppBundle\Service;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Entity\UserComicRepository;
use AppBundle\Entity\ComicRepository;

class Comics
{

    private $token;
    private $userComicRepository;
    private $comicRepository;
    private $marvelAPISearch;

    public function __construct(
        TokenStorage $token,
        UserComicRepository $userComicRepository,
        ComicRepository $comicRepository,
        MarvelAPISearch $marvelAPISearch) {
        $this->token = $token;
        $this->userComicRepository = $userComicRepository;
        $this->comicRepository = $comicRepository;
        $this->marvelAPISearch = $marvelAPISearch;
    }

    public function fetchComicDetails($comicDetails) {
        $comics = array();
        foreach ($comicDetails->data->results as $comic) {
            $userComic = $this->userComicRepository->findOneBy(
                array(
                    'user' => $this->token->getToken()->getUser(),
                    'comicId' => $comic->id
                )
            );
            $comics[] = array(
                'title' => $comic->title,
                'id' => $comic->id,
                'read' => $userComic ? $userComic->getIsRead() : 0,
                'watched' => $userComic ? $userComic->getWatch() : 0,
                'favourite' => $userComic ? $userComic->getFavourite() : 0
            );
        }
        return $comics;
    }

    public function populateLocalComicData($commit = false) {
        $offset = 0;
        if ($commit) {
            do {
                $comics = $this->marvelAPISearch->fullComicSearch(null, $offset);
                $total = $comics->data->total;
                if(count($comics->data->results)) {
                    foreach($comics->data->results as $comic) {
                        $localComic = $this->comicRepository->findOneBy(
                            array('marvelId' => $comic->id)
                        );
                        if (!$localComic) {
                            $this->comicRepository->addComic($comic);
                        }
                    }
                }
                $offset = $offset + 1;
            } while ($comics->data->results > 0);
        } else {
            $comics = $this->marvelAPISearch->fullComicSearch(null, $offset);
            $total = $comics->data->total;
        }
        return $total;
    }
}