<?php

/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 20/04/2016
 * Time: 3:48 PM
 */

Namespace AppBundle\Service;

use Buzz\Browser;
use Buzz\Client\Curl;

class MarvelAPISearch
{

    protected $publicKey;
    protected $privateKey;
    protected $gateway;

    private $maxPerPage;

    public function __construct($publicKey, $privateKey, $marvelGateway) {
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;
        $this->gateway = $marvelGateway;
        $this->maxPerPage = 20;
    }

    private function queryMarvelApi($entity, $searchTerm = null, $formData = null, $comics = false) {
        $ts = time();
        $hash = md5($ts.$this->privateKey.$this->publicKey);
        $parameters = array(
            'ts='.$ts,
            'apikey='.$this->publicKey,
            'hash='.$hash
        );
        if ($comics) {
            $parameters = array_merge($parameters, array(
                'hasDigitalIssue=true',
                'noVariants=true',
                'format=comic'
            ));
        }
        if ($entity == 'series') {
            $parameters = array_merge($parameters, array(
                'contains=comic'
            ));
        }

        /*
         * Handle form data for search
         */
        if ($formData) {
            $processedFormData = $this->processFormData($formData);
            $parameters = array_merge($parameters, $processedFormData); // Add the search form data to the query string
        }
        $curl = new Curl();
        // Enable GZIP
        $curl->setOption(CURLOPT_ENCODING, 'gzip');
        $headers = array(
            'Accept-Encoding' => 'gzip'
        );
        $browser = new Browser();
        $browser->setClient($curl);
        $browser->getClient()->setTimeout(10000);
        $url = $this->gateway.$entity;
        if ($searchTerm) {
            $url = $url."/".urlencode($searchTerm);
        }
        $url = $url.'?'.implode('&', $parameters);
        $results = $browser->get($url,$headers);

        $data = json_decode($results->getContent());

        if ($data->code != 200) {
            throw new \Exception('The Marvel API reported an error: '.$data->status );
        }

        return $data;
    }

    public function processFormData($formData) {
        $processedFormData = array();
        foreach ($formData as $formField => $formFieldValue) {
            if ($formFieldValue) {
                $processedFormData[] = $formField.'='.urlencode($formFieldValue);
            }
        }
        return $processedFormData;
    }

    /*
     * Comics
     */

    public function fullComicSearch($data, $page) {
        $entity = 'comics';
        $offset = 0;
        if ($page > 1) {
            $offset = ($page - 1) * 100; // 100 is the max allowed by Marvel
        }
        $data = array('offset' => $offset, 'limit' => $this->maxPerPage);
        $comics = $this->queryMarvelApi($entity, null, $data, true);
        return $comics;
    }

    public function comicSearch($comicId) {
        $entity = "comics/$comicId";
        $comic = $this->queryMarvelApi($entity);
        return $comic;
    }

    /*
     * Series
     */

    public function seriesSearch($data, $page) {
        $entity = 'series';
        $offset = 0;
        if ($page > 1) {
            $offset = ($page - 1) * $this->maxPerPage;
        }
        $data = array('titleStartsWith' => $data, 'offset' => $offset, 'limit' => $this->maxPerPage);
        $series = $this->queryMarvelApi($entity, null, $data);
        return $series;
    }

    public function fetchSeries($seriesId) {
        $entity = "series/$seriesId";
        $series = $this->queryMarvelApi($entity);
        return $series;
    }

    public function comicSeriesSearch($data, $page) {
        $entity = 'series/'.$data.'/comics';
        $offset = 0;
        if ($page > 1) {
            $offset = ($page - 1) * $this->maxPerPage;
        }
        $data = array('orderBy' => 'onsaleDate', 'offset' => $offset, 'limit' => $this->maxPerPage);
        $comics = $this->queryMarvelApi($entity, null, $data, true);
        return $comics;
    }

    /*
     * Events
     */

    public function eventSearch($data, $page) {
        $entity = 'events';
        $offset = 0;
        if ($page > 1) {
            $offset = ($page - 1) * $this->maxPerPage;
        }
        $data = array('nameStartsWith' => $data, 'offset' => $offset, 'limit' => $this->maxPerPage);
        $series = $this->queryMarvelApi($entity, null, $data);
        return $series;
    }

    public function fetchEvent($eventId) {
        $entity = "events/$eventId";
        $series = $this->queryMarvelApi($entity);
        return $series;
    }

    public function comicEventSearch($data, $page) {
        $entity = 'events/'.$data.'/comics';
        $offset = 0;
        if ($page > 1) {
            $offset = ($page - 1) * $this->maxPerPage;
        }
        $data = array('orderBy' => 'onsaleDate', 'offset' => $offset, 'limit' => $this->maxPerPage);
        $comics = $this->queryMarvelApi($entity, null, $data, true);
        return $comics;
    }
}