<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 12/05/2016
 * Time: 10:20 AM
 */

namespace AppBundle\Service;

use AppBundle\Entity\ComicRepository;
use AppBundle\Entity\UserComic;

class MyComics
{

    private $comicsRepository;

    public function __construct(ComicRepository $comicRepository) {
        $this->comicsRepository = $comicRepository;
    }

    /**
     * Sort user's comics into the right sections
     * @param UserComic[] $userComics
     * @return mixed
     */
    public function sortMyComics($userComics) {
        $myComics['comicsRead'] = array();
        $myComics['comicsWatched'] = array();
        $myComics['comicsFavourited'] = array();
        foreach($userComics as $userComic) {
            $comicDetails = $this->comicsRepository->findOneBy(array(
                'marvelId' => $userComic->getComicId()
            ));
            if ($comicDetails) {
                if ($userComic->getIsRead()) {
                    $myComics['comicsRead'][$comicDetails->getSeries()][] = $comicDetails;
                }
                if ($userComic->getWatch()) {
                    $myComics['comicsWatched'][$comicDetails->getSeries()][] = $comicDetails;
                }
                if ($userComic->getFavourite()) {
                    $myComics['comicsFavourited'][$comicDetails->getSeries()][] = $comicDetails;
                }
            }
        }
        return $myComics;
    }

}