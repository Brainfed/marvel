<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 18/05/2016
 * Time: 4:56 PM
 */

namespace AppBundle\Service;


class PageData
{

    public function parsePageData($search, $results, $page, $comics = null, $searchKind = 'series') {
        $pageData = array(
            'results' => $results->data->results,
            'total' => $results->data->total,
            'count' => $results->data->count,
            'pages' => ceil($results->data->total / $results->data->limit),
            'page' => $page,
            'searchKind' => $searchKind,
            'search' => $search,
            'offset' => $results->data->offset,
            'comics' => $comics,
            'attribution' => $results->attributionHTML
        );
        return $pageData;
    }
}