<?php
$db = parse_url(getenv('CLEARDB_DATABASE_URL'));

$container->setParameter('database_driver', 'pdo_mysql');
$container->setParameter('database_host', $db['host']);
$container->setParameter('database_port', $db['port']);
$container->setParameter('database_name', substr($db["path"], 1));
$container->setParameter('database_user', $db['user']);
$container->setParameter('database_password', $db['pass']);
$container->setParameter('secret', getenv('SECRET'));
$container->setParameter('locale', 'en');
$container->setParameter('mailer_transport', null);
$container->setParameter('mailer_host', null);
$container->setParameter('mailer_user', null);
$container->setParameter('mailer_password', null);

# Marvel Settings
$container->setParameter('marvel_api_public_key', getenv('MARVEL_API_PUBLIC_KEY'));
$container->setParameter('marvel_api_private_key', getenv('MARVEL_API_PRIVATE_KEY'));
$container->setParameter('marvel_gateway', getenv('MARVEL_GATEWAY'));