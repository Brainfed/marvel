<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160519033025 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comic CHANGE marvel_id marvel_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX marvel_id ON comic');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5B7EA5AA48CA5102 ON comic (marvel_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comic CHANGE marvel_id marvel_id INT NOT NULL');
        $this->addSql('DROP INDEX uniq_5b7ea5aa48ca5102 ON comic');
        $this->addSql('CREATE UNIQUE INDEX marvel_id ON comic (marvel_id)');
    }
}
