<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160510075111 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE comic (id INT AUTO_INCREMENT NOT NULL, marvel_id INT NOT NULL, title VARCHAR(255) NOT NULL, issue_number INT NOT NULL, series VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_comic DROP INDEX IDX_B9BC5EF2A76ED395, ADD UNIQUE INDEX UNIQ_B9BC5EF2A76ED395 (user_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE comic');
        $this->addSql('ALTER TABLE user_comic DROP INDEX UNIQ_B9BC5EF2A76ED395, ADD INDEX IDX_B9BC5EF2A76ED395 (user_id)');
    }
}
