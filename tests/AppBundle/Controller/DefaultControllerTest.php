<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Marvel Unlimited Companion', $crawler->filter('#frontpage h1')->text());
    }

    public function testAbout()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/about/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertContains('Marvel Unlimited Companion', $crawler->filter('#aboutpage h1')->text());

        $this->assertContains('Made by Matt Hamm', $crawler->filter('#aboutpage h2')->text());

        $this->assertContains('A work in progress', $crawler->filter('#aboutpage h3')->text());
    }
}
