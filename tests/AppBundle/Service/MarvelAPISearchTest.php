<?php
/**
 * Created by PhpStorm.
 * User: matthew.hamm
 * Date: 12/05/2016
 * Time: 2:53 PM
 */

namespace tests\AppBundle\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Service\MarvelAPISearch;

class MarvelAPISearchTest extends WebTestCase
{

    public function testProcessFormData() {
        $formData = array(
            'test' => 'result',
            'is' => 'positive',
        );
        $marvelAPISearch = new MarvelAPISearch(null, null, null);
        $formData = $marvelAPISearch->processFormData($formData);
        $this->assertEquals(
            array(
                'test=result',
                'is=positive'
            ),
            $formData
        );
    }

}