Marvel Unlimited Companion
==================

A Symfony project to keep track of comics you've read and add ratings for them.
The Marvel Unlimited app doesn't let you do this (right now) so I created this app for myself.

It uses the Marvel API to fetch comic/character/series information.

Caches some Marvel data locally but not long term due to the terms of the Marvel API.

To configure just add your public and private API keys to app/config/parameters.yml

marvel_api_public_key: <public_key>
marvel_api_private_key: <private_key>
